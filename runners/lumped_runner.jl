using PyPlot
using Parameters
using DifferentialEquations,OrdinaryDiffEq
import Roots: find_zero
using StatsBase
using Dates

glacier ="synth_evol"

include("../src/interface_lumped.jl")
include("../src/interface-$(glacier).jl")
include("../src/$(glacier)_lump.jl") # 

if homedir()== "/Users/Ian"
    fp_save = "/Users/Ian/julia/lumped_runner/"
end

if glacier == "synth_evol"
    pg = LumpSynthEvol(pg,
                       ϵ̇=1e-3/year,
                       S0 = 1, 
                       ht0=[0.10, 0.290],
                       hd = 0.00)
    
else
    ## parameters are around the best model runs
    pg = LumpedRealGlacierEvol(pg,
                               ϵ̇=.0e-3/year,
                               ht0=[.1, 0.00],
                               hd=0.00
                               )
end


println("starting run_model()")
println(Dates.now())
@time sol  = run_model(pg, pp, pn)
println("
    run_model() finished
    ")

hts=sol(pg.tout)
hts = hcat(hts...)'
ė=zeros(length(pg.tout)); Qbs=zeros(length(pg.tout),2);Qbes=zeros(length(pg.tout),2);Qb_d=zeros(length(pg.tout));  Qws=zeros(length(pg.tout))
S=zeros(length(pg.tout))
for k in eachindex(pg.tout)
    t = pg.tout[k]
    Qbs[k,:] = -dht_dt_fn_lump(hts[k,:], t, pg, pp, pn)[2]
    Qb_d[k]  = dht_dt_fn_lump(hts[k,:], t, pg, pp, pn)[4][1]
    Qbes[k,:] = -dht_dt_fn_lump(hts[k,:], t, pg, pp, pn)[3]    
    Qws[k] = -dht_dt_fn_lump(hts[k,:], t, pg, pp, pn)[5][1]
end

start= hts[1,1]  *DS(pg.tout[1],pg)  *pg.dh + hts[1,2]  *DS_p(pg.tout[1],pg)  *pg.dh_p
fin =  hts[end,1]*DS(pg.tout[end],pg)*pg.dh + hts[end,2]*DS_p(pg.tout[end],pg)*pg.dh_p


clf()
figure(2)
ax=subplot(3,1,1)
plot(pg.tout./year,hts[:,1],label="subglacial")
plot(pg.tout./year,hts[:,2],label="proglacial")
ylabel("H \n (m)")
legend()

subplot(3,1,2,sharex=ax)

plot(pg.tout./year,Qbs[:,1])
plot(pg.tout./year,Qbs[:,2])
plot(pg.tout./year,Qb_d)
ylabel("Qₛ \n (m³s⁻¹)")

subplot(3,1,3,sharex=ax)
plot(pg.tout./year,Qws)
ylabel(L"Q$_w$ (m³s⁻¹)")
xlabel("year")
tight_layout()
subplots_adjust(hspace=-0.0)
