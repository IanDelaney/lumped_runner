using Pkg
Pkg.activate(".")
using Parameters
using Random
using Distributions
using DifferentialEquations,OrdinaryDiffEq
using StatsBase
using JLD2 
using Dates
using Random

glacier = "fiescher_evol"

include("../src/interface_lumped.jl")
include("../src/interface-$(glacier).jl")
include("../src/$(glacier)_lump.jl")

max_min = [0.035 .05 ## 
           0.06e-3/year 1.4e-3/year # bottom of hallet1996's estimates and max is from Aletsch analysis in delaney 2018
           0.0005 .05 # difference between h0p and h0g due
           0.0005 .05
           0.0005 .05
           ]

bulk_no = 50
block_no = 25000
node =  1

prob = (p) -> objfun(pg,pp,pn,p)

obs = time_scale_sum(max.(1e-11,pg.sediment(pg.tout)),pg.tout,pg.time_agg,pg) #max added to account for negative values from splining

include("../src/parameter_search_pmap.jl")

for i =1:bulk_no
    runs= Int(block_no + i)

    cost, thetas = parameter_search_lumped(prob,obs,max_min,runs)

    JLD2.@save "$(glacier)_$(runs)_$(Int(node))_lump_glue_260423_hehphg_log.jld2" {compress=true}  cost thetas obs max_min
end

