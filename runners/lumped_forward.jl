using PyPlot
using Pkg
Pkg.activate(".")
using Parameters
using Random
using Distributions
using DifferentialEquations,OrdinaryDiffEq
import Roots: find_zero
using StatsBase
using JLD2 
using Dates
using Random

glacier = "fiescher_evol"

include("../src/interface_lumped.jl")
include("../src/interface-$(glacier).jl")
include("../src/$(glacier)_lump.jl")

@load "theta_fiescher_evol_select.jld2" theta costs

obs = time_scale_sum(max.(1e-4,pg.sediment(pg.tout)),pg.tout,5*day,pg)[:,1]

thetas = pull_thetas(theta[:,:])
println(size(thetas))
prob = (p) -> objfun(pg,pp,pn,p;forward=true)

tout= pg.tout
model_outs=zeros(length(pg.tout[1:20:end-20]),size(thetas,1))
Qs_pga=zeros(length(pg.tout),size(thetas,1))
Qs_sga=zeros(length(pg.tout),size(thetas,1))
Qbes_sga =zeros(length(pg.tout),size(thetas,1))
Qbes_pga =zeros(length(pg.tout),size(thetas,1))
Qb_dg = zeros(length(pg.tout),size(thetas,1))
ht_pga=zeros(length(pg.tout),size(thetas,1))
ht_sga=zeros(length(pg.tout),size(thetas,1))

Qws =  abs.(Qw.(pg.tout,Ref(pg),Ref(pp)))
water_outs =  time_scale_sum(Qws,pg.tout,pg.time_agg,pg)[:,1]
for i =1:size(thetas,1)

    @time  model,  Qbs, hts, Qbes, Qb_d  = prob(thetas[i,:])
    
    model_outs[:,i]=model
    Qs_pga[:,i]  = Qbs[:,2]
    Qs_sga[:,i]  = Qbs[:,1]
    Qbes_pga[:,i]= Qbes[:,2]
    Qbes_sga[:,i]= Qbes[:,1]
    ht_pga[:,i]  = hts[:,2]
    ht_sga[:,i]  = hts[:,1]
    Qb_dg[:,i]   = Qb_d
    println("run $i of $(size(thetas,1)). minimum ht_s $(minimum(ht_sga)) -> cost: $(costs[i]) ")
end

@save "outputs_fiescher_evol_select.jld2" theta costs tout obs Qws water_outs model_outs Qs_pga Qs_sga Qbes_sga Qbes_pga Qb_dg  ht_pga ht_sga 

