using PyPlot,JLD2,StatsBase
#year = 365*86400

include("plotting_funs.jl")
include("../src/interface_lumped.jl")
#take min of percent or no samples
percent = 0.0005 # in paper 
nsamples =600

@load "all_runs_thetas_costs.jld2" thetas cost

max_min = [0.035 .052 ## 
           0.06e-3/year 1.4e-3/year # bottom of hallet1996's estimates and max is from Aletsch analysis in delaney 2018
           0.0005 .05 # difference between h0p and h0g due
           0.0005 .05
           0.0005 .05
           ]

#prep data
index= []
cost_= []

for i = 1:length(cost[:,1])
    push!(index,i)
    push!(cost_,  cost[i,2])
end
#theta=hcat(theta...)
tmp=sortslices([cost_ index],dims=1)
inde= min(nsamples,Int(floor(percent*size(cost)[1])))

index = tmp[1:inde,2]
costs = tmp[1:inde,1]
theta = thetas[index,:]

@save "theta_fiescher_evol_select.jld2" theta costs

thetas[:,2]= thetas[:,2]*1e3*year
theta[:,2] = theta[:,2]*1e3*year
max_min[2,:] =max_min[2,:] *1e3*year

prior = 0 .* theta 
for i =1:nsamples
    prior[i,:] .= sampler_(max_min)
end


no_bins=30
t_lim = 2.581 # 95% confidence
marks_p =zeros(no_bins+1,size(theta,2))
ysp =zeros(no_bins,size(theta,2))
for i in eachindex(theta[1,:])
    ysp[:,i] ,marks_p[:,i],_ = hist(prior[:,i],bins=no_bins)
end
clf()

marks_p[:,3] = 10 .^range(log10(marks_p[1,3]),log10(marks_p[end,3]),no_bins+1)
ysp[:,3] ,marks_p[:,3],_ = hist(prior[:,3],bins=marks_p[:,3])
marks_p[:,4] = 10 .^range(log10(marks_p[1,4]),log10(marks_p[end,4]),no_bins+1)
ysp[:,4] ,marks_p[:,4],_ = hist(prior[:,4],bins=marks_p[:,4])
marks_p[:,5] = 10 .^range(log10(marks_p[1,5]),log10(marks_p[end,5]),no_bins+1)
ysp[:,5] ,marks_p[:,5],_ = hist(prior[:,5],bins=marks_p[:,5])




clf()

labels = [
    L"$D_{50}$"*"\n (m)",
    
    L"$\dot{\epsilon}$"*"\n (mm a⁻¹)",
    L"$H_{g0}$"*"\n (m)",
    L"$H_{p0}$"*"\n (m)",
    L"$H_e$"*"\n (m) "]

##############################################
##############################################
##############################################
##############################################

figure("c",figsize=(7,5.5))
clf()
i =1
nx=size(theta,2)
cmap = get_cmap("bwr")
for i = 1:nx
    for j=1:nx
        ij = i+(j-1)* nx
        subplot(nx,nx,ij)
        ### histograms on diagonal
        if i ==j

            axhspan(0,mean(ysp[:,i]),color="grey",alpha=.75)
            
            if i >=3
                hist(theta[:,i],bins=marks_p[:,i])
                xscale("log")
            else
                hist(theta[:,i],bins=marks_p[:,i])
            end

            axvline(mean(theta[:,i]),color="red")
            if i ==1
                text(1.02*mean(theta[:,i]), 15, "μ: $(round(mean(theta[:,i]),digits=3))", rotation = 270)
            else
                text(1.1*mean(theta[:,i]), 15, "μ: $(round(mean(theta[:,i]),digits=3))", rotation = 270)
            end
                # if i == 1
            #         text(0.031,50,
            #              "no. samples: 
            # $(inde)")
            #     end
            
            
            ### get axis sorted
            #yticks([])
            xlim([minimum(max_min[i,:]),maximum(max_min[i,:])])
            
        else
            if i > j
                corr= cor(theta[:,i],theta[:,j])
                n = length(theta[:,1])
                t=corr /(sqrt((1-corr^2)/(n-2)))
                if abs(t) >t_lim
                    
                    axhspan(minimum(max_min[i,:]),maximum(max_min[i,:]),color=cmap(1-(corr+1)/2))
                else
                    axhspan(minimum(max_min[i,:]),maximum(max_min[i,:]),color=cmap(1-(0+1)/2))
                end
                
                text(mean(max_min[i,:]), mean(max_min[i,:]), round(corr,digits=2))
                #text(mean(max_min[i,:]), mean(max_min[i,:]), "$i $j")
                xlim([minimum(max_min[i,:]),maximum(max_min[i,:])])
                ylim([minimum(max_min[i,:]),maximum(max_min[i,:])])
                xscale("linear"); yscale("linear")
                ### scatter plots
            else

                if i>=3 && j>=3
                    loglog(theta[:,i],theta[:,j],"*",markersize=.1,color="grey")
                elseif j >=3
                    semilogy(theta[:,i],theta[:,j],"*",markersize=.1,color="grey")
                else
                    
                    plot(theta[:,i],theta[:,j],"*",markersize=.1,color="grey")
                end
                xlim([minimum(max_min[i,:]),maximum(max_min[i,:])])
                ylim([minimum(max_min[j,:]),maximum(max_min[j,:])])
            end
        end
        
        

        
        
        if i == 1 && j==nx
            xlabel(labels[i])
            ylabel(labels[j])
            yticks(rotation=45)
            xticks(rotation=45)
        elseif i ==1
            ylabel(labels[j])
            yticks(rotation=45)
            if j==1
                yticks([])
            end
        elseif j == nx
            xlabel(labels[i])
            xticks(rotation=45)
            yticks([])
        else
            
            xticks([]);yticks([])
        end
        
    end
    

end
tight_layout()
subplots_adjust(hspace=-0.0,wspace=0.0)

savefig("/Users/Ian/research/lumped_model/manuscript/figs/corr_plot.png")


###########################################
###########################################
###########################################
plot_lab= ["a" "b" "c" "d" "e" "f" "g" "h";
           "i" "j" "k" "l" "m" "n" "o" "p" ]




nrow = 3
ncol = size(theta,2)
figure("b",figsize=(5.5,4))
clf()
for i = 0:size(theta,2)-1
    
    subplot2grid((nrow,ncol), (0,i), rowspan=1)

    if i>=2
        tmp, _ = hist(theta[:,i+1],bins=marks_p[:,i+1])
        xscale("log")
    else
        tmp, _ = hist(theta[:,i+1],bins=marks_p[:,i+1])
    end
    axhspan(0,mean(ysp[:,i+1]),color="grey",alpha=.75)
    axvline(mean(theta[:,i+1]),color="red")
    text(1.05*mean(theta[:,i+1]), 500, "μ: $(round(mean(theta[:,i+1]),digits=3))", rotation = 270)
    xlim([minimum(max_min[i+1,:]),maximum(max_min[i+1,:])])
    text(1.01*max_min[i+1,1],0.9*maximum(tmp), plot_lab[1,i+1])
    
    ### get axis sorted
    yticks([])
    xticks([])

    
    subplot2grid((nrow,ncol), (1,i), rowspan=2)
    if i >= 2
        semilogx(theta[:,i+1],costs,ms=2,"*",color="grey")
    else
        plot(theta[:,i+1],costs,ms=2,"*",color="grey")
    end
    text(1.01*max_min[i+1,1],58500, plot_lab[2,i+1])
    
    xlim([minimum(max_min[i+1,:]),maximum(max_min[i+1,:])])
    #   xlim([-.5, 1])
    xlabel(labels[i+1])
    xticks(rotation=45)
    ylim([maximum(costs), minimum(costs)])
    
    if i ==0
        
        
        ylabel("Absolute Error (ξ; m³)")
        
        yticks(rotation = 45)
    else
        yticks([])
        tick_params(
            axis="y",          # changes apply to the x-axis
            which="both",      # both major and minor ticks are affected
            bottom=false,      # ticks along the bottom edge are off
            top=false,         # ticks along the top edge are off
            left=false,
            right=false,
            labelbottom=false) # labels along the bottom edge are off
        
    end
    

end
tight_layout()
subplots_adjust(hspace=-0.0,wspace=0.0)

savefig("/Users/Ian/research/lumped_model/manuscript/figs/para_space.pdf")
