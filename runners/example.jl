using PyPlot
using Parameters
using DifferentialEquations,OrdinaryDiffEq
import Roots: find_zero
using StatsBase
using Dates

glacier = "synth"

include("../src/interface_lumped.jl")
include("../src/interface-$(glacier).jl")
include("../src/$(glacier)_lump.jl") # 

if homedir()== "/Users/Ian"
    fp_save = "/Users/Ian/julia/lumped_runner/"
end

pg= LumpSynthEvol(tspan=(0*year,5*year))
pg= LumpSynthEvol(pg,
                  source_average_time=5*day,
                  ht0=[.0015, 0.001],   # initial till conditions
                  tout=pg.tspan[1]:.25*day:pg.tspan[2],
                  ϵ̇=1e-3/year, # background erosion rate
                  hd = .01 # englacial debris concentration
                  )

pp=Phys(Dm=.005, # sediment grain size
        DDAMP=1, # Diurnal amplitude in temperature
        DAMP=16, #  Annual amplitude in temperature
        ΔT=2.0,  # Temperature offset
        )

retreat_rate(t) = 100/year              # retreat rate of the glacier  
thinning(t)= 2/year*(t-pg.tspan[1])    # Thinning rate
proglacial_slope(t) = 0*t/year         ## change in slope in the proglacial area
dl_dt(t,pg) =0
dht_dt_fn(ht, t, pg, pp, pn) =dht_dt_fn_lump(ht, t, pg, pp, pn)
gradzb_fluvial(t,ht,pg,pn) = 0.02
DS_p(t,pg)=2000

println("starting run_model()")
println(Dates.now())
@time sol  = run_model(pg, pp, pn)
println("
    run_model() finished
    ")

hts=sol(pg.tout)
hts = hcat(hts...)'
ė=zeros(length(pg.tout)); Qbs=zeros(length(pg.tout),2);Qbes=zeros(length(pg.tout),2);Qb_d=zeros(length(pg.tout));  Qws=zeros(length(pg.tout))
S=zeros(length(pg.tout))
for k in eachindex(pg.tout)
    t = pg.tout[k]
    Qbs[k,:] = -dht_dt_fn_lump(hts[k,:], t, pg, pp, pn)[2]
    Qb_d[k]  = dht_dt_fn_lump(hts[k,:], t, pg, pp, pn)[4][1]
    Qbes[k,:] = -dht_dt_fn_lump(hts[k,:], t, pg, pp, pn)[3]    
    Qws[k] = -dht_dt_fn_lump(hts[k,:], t, pg, pp, pn)[5][1]
end



clf()
figure(2)
ax=subplot(3,1,1)
plot(pg.tout./year,hts[:,1],label="subglacial")
plot(pg.tout./year,hts[:,2],label="proglacial")
ylabel("H \n (m)")
legend()

subplot(3,1,2,sharex=ax)

plot(pg.tout./year,Qbs[:,1])
plot(pg.tout./year,Qbs[:,2])
plot(pg.tout./year,Qb_d)
ylabel("Qₛ \n (m³s⁻¹)")

subplot(3,1,3,sharex=ax)
plot(pg.tout./year,Qws)
ylabel(L"Q$_w$ (m³s⁻¹)")
xlabel("year")
tight_layout()
subplots_adjust(hspace=-0.0)
