using Pkg
Pkg.activate(".")
using PyPlot
using Parameters
using DifferentialEquations,OrdinaryDiffEq
import Roots: find_zero

using StatsBase
using Dates

glacier = "synth_glacier"

include("../src/interface_lumped.jl")
include("../src/interface-$(glacier).jl")
include("../src/$(glacier)_lump.jl") # 

if homedir()== "/Users/Ian"
    fp_save = "/Users/Ian/julia/lumped_runner/"
end

pg= LumpSynthEvol(tspan=(0*year,1*year))
pg= LumpSynthEvol(pg,
                  ht0=[.001],   # initial condition-till height
                  tout=pg.tspan[1]:.25*day:pg.tspan[2], # read output at every quarter of a day
                  ϵ̇=1e-3/year, # background erosion rate
                  
                  )

pp=Phys(pp,
        hookeangle = π/2, # controls channel shape. see Hooke et al., 
        formula =  :E_H, # W_C is Wilcock and Crow (bedload) switch to E_H for Engelund and Hansen (suspended)
        Dm=.01, # sediment grain size
        DDAMP=.5, # Diurnal amplitude in temperature
        DAMP=16, #  Annual amplitude in temperature
        ΔT=2.0,  # Temperature offset
        
        )

println("starting run_model()")
println(Dates.now())
@time sol  = run_model(pg, pp, pn)
println("
    run_model() finished
    ")

hts=sol(pg.tout)
hts = hcat(hts...)'
S = sol(pg.tout)[2,:]
hts = hts[:,1]
ė=zeros(length(pg.tout)); Qbs=zeros(length(pg.tout));Qbes=zeros(length(pg.tout));  Qws=zeros(length(pg.tout))

for k in eachindex(pg.tout)
    _, Qbs[k], Qbes[k], Qws[k] = dht_dt_fn(sol(pg.tout[k]), pg.tout[k], pg, pp, pn)
end
Qbs=abs.(Qbs); Qws=abs.(Qws); Qbes=abs.(Qbes)

clf()
figure(2)
ax=subplot(3,1,1)
plot(pg.tout./year,hts,label="subglacial")
ylabel("H \n (m)")
legend()

subplot(3,1,2,sharex=ax)
axb =plot(pg.tout./year,Qbes,color="orange")
ylabel("Qₛₑ\n (m³s⁻¹)",color="orange")

ax2 =twinx()

plot(pg.tout./year,Qbs)
ylabel("Qₛ \n (m³s⁻¹)")

subplot(3,1,3,sharex=ax)
plot(pg.tout./year,Qws)
ylabel(L"Q$_w$ (m³s⁻¹)")
xlabel("year")
tight_layout()
subplots_adjust(hspace=-0.0)
