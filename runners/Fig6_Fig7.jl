using PyPlot
#pygui(:tk)
using StatsBase
using JLD2 
using Dates
include("plotting_funs.jl")
dy = 86400
yr = dy*365

@load "outputs_fiescher_evol_select.jld2" theta costs tout obs model_outs water_outs Qws Qs_pga Qs_sga Qbes_sga Qbes_pga Qb_dg  ht_pga ht_sga 
cormod= cor(model_outs,obs)
corw= cor(water_outs,obs)
dt = tout[2]- tout[1]

println(
"maximum cor: $(maximum(cormod))
minimum cor: $(minimum(cormod))
mean cor: $(mean(cormod))
median cor: $(median(cormod))


maximum cor: $(maximum(corw))
minimum cor: $(minimum(corw))
mean cor: $(mean(corw))
median cor: $(median(corw))

mean Qe : $(mean(sum(-Qb_dg,dims=1)*dt))
maximum Qe : $(maximum(sum(-Qb_dg,dims=1)*dt))
minimum Qe : $(minimum(sum(-Qb_dg,dims=1)*dt))


mean Qp : $(mean(sum(-Qs_pga,dims=1)*dt))
maximum Qp : $(maximum(sum(-Qs_pga,dims=1)*dt))
minimum Qp : $(minimum(sum(-Qs_pga,dims=1)*dt))

        ")


nruns=length(costs)
nbins=20
ratio = (Qs_sga )./(Qs_pga)

for i in eachindex(Qs_sga)
    if Qs_sga[i] * (5*dy) < 100    
        ratio[i] =NaN
    end
end
mean_Qbs = mean(Qs_sga,dims=2)
mean_Qbs_pga = mean(Qs_pga,dims=2)
band_Qbs = zeros(length(mean_Qbs),2)
for i = 1:length(mean_Qbs)
    band_Qbs[i,1] =  quantile!(Qs_sga[i,:], .9999)
    band_Qbs[i,2] =  quantile!(Qs_sga[i,:], .0001)
end
mean_Qb_dg = mean(-Qb_dg,dims=2)
band_Qb_dg = zeros(length(mean_Qb_dg),2)
for i = 1:length(mean_Qbs)
    band_Qb_dg[i,1] =  quantile!(-Qb_dg[i,:], .9999)*yr
    band_Qb_dg[i,2] =  quantile!(-Qb_dg[i,:], .00001)*yr
end
mean_ratio = mean(ratio,dims=2)
band_ratio = zeros(length(mean_ratio),2)
band_ratio[:,1] = maximum(ratio,dims=2)
band_ratio[:,2] = minimum(ratio,dims=2)

mean_model_outs = mean(model_outs,dims=2)./(5*dy)
band_model_outs = zeros(length(mean_model_outs),2)
band_model_outs[:,1] = maximum(model_outs,dims=2)./(5*dy)
band_model_outs[:,2] = minimum(model_outs,dims=2)./(5*dy)

save= true
figure("Outputs",figsize=(8,5))
clf()
ax=subplot(3,2,1)
plot(tout./yr .+ 1970,Qws,"black", lw=1)
ylim([.1, 1.05*maximum(Qws)])
#xticks([])
ylabel("Water discharge \n (m³ s⁻¹)")
xlim([2014, 2020.99])
xticks(rotation=90,fontsize=0.000001)
text(2014.25,29,"a")

subplot(3,2,3,sharex=ax)
#plot(tout./yr .+ 1970,Qs_sga,"red",lw=0.1)
fill_between(tout./yr .+ 1970,band_Qbs[:,1],band_Qbs[:,2],color="red")
ylim([.000001,0.0275])
ylabel("Subglacial \n sediment flux \n"*L"($Q_{g}$; m³ s⁻¹)")
xticks(rotation=90,fontsize=0.000001)
text(2014.25,0.025,"b")

ax_deb=subplot(3,2,5,sharex=ax)
ylabel("Emergent \n sediment flux\n "*L"(Q_{e};"*"m³ a⁻¹)",color="green")
fill_between(tout./yr .+ 1970,band_Qb_dg[:,1],band_Qb_dg[:,2],color="green",alpha=.75)
ylim([1e-7, 2600])
xlabel("Year")
plt=twinx()

xticks(rotation=45)
fill_between(tout[10:20:end-10]./yr .+ 1970,band_model_outs[:,1],band_model_outs[:,2],color="blue")
plot(tout[10:20:end-10]./yr .+ 1970,obs./(5*dy),"orange",lw=1.5,alpha=.9)
#plot(tout[1:20:end-20]./yr .+ 1970,model_outs,"blue", lw=.05)
ylim([minimum(obs)./(5*dy), 0.013])
text(2014.25,0.0115,"c")

ax=subplot(3,2,2)
plot(tout./yr .+ 1970,Qws,"black", lw=1)
ylim([.1, 1.05*maximum(Qws)])
yticks([])
#ylabel("Water discharge \n (m³ s⁻¹)")
xlim([2017.25, 2017.81])
text(2017.27,29,"d")
xticks(rotation=90,fontsize=0.000001)


subplot(3,2,4,sharex=ax)

fill_between(tout./yr .+ 1970,band_Qbs[:,1],band_Qbs[:,2],color="red")
#plot(tout./yr .+ 1970,mean_Qbs,"red",lw=0.1)
ylim([.000001,0.0275])
yticks([])
text(2017.27,0.025,"e")
xticks(rotation=90,fontsize=0.000001)

subplot(3,2,6,sharex=ax)
yticks([])
fill_between(tout./yr .+ 1970,band_Qb_dg[:,1],band_Qb_dg[:,2],color="green")
ylim([1e-7, 2600])
xlabel("Year (2017)")
plt=twinx()



xticks(rotation=45)
ylim([minimum(obs)./(5*dy), 0.013])



plot(tout[10:20:end-10]./yr .+ 1970,model_outs[:,1].+32324312423,"blue", lw=.5,label="model")
plot(tout[10:20:end-10]./yr .+ 1970,obs .+1000000,"orange",lw=1.5,label="observation")
fill_between(tout[10:20:end-10]./yr .+ 1970,band_model_outs[:,1],band_model_outs[:,2],color="blue")
fill_between(tout./yr .+ 1970,band_Qb_dg[:,1],band_Qb_dg[:,2],color="green",alpha=.75)
plot(tout[10:20:end-10]./yr .+ 1970,obs./(5*dy),"orange",lw=1.5)
ylim([minimum(obs)./(5*dy), 0.013])
ylabel("Sediment discharge \n"* L"($Q_p$, Q "*"; m³ s⁻¹)",rotation = 270,labelpad=30)
text(2017.27,0.0115,"f")
legend(loc=2)

tight_layout()
subplots_adjust(hspace=0.0,wspace=0.0)

if save == true
    savefig("/Users/Ian/research/lumped_model/model_outputs/model_outputs_forward_tot.pdf")
end

figure("Process",figsize=(8,5))

axb=subplot(3,1,1)
axhspan(0, 1, color="lightgray")
fill_between(tout./yr .+ 1970,band_ratio[:,1],band_ratio[:,2],color="orange")
xticks(rotation=90,fontsize=0.000001)
text(2014.25,1.1,"proglacial deposition")
text(2014.25, .8,"proglacial erosion")
ylim([.3,1.4])

ylabel("Ratio Qₛ\n" * L" ($\frac{Q_{g}}{Q_{p}}$)")
text(2014.25,1.27,"a",fontsize=14)

subplot(3,1,2,sharex=axb)
lim=0.005
tmp = findall(x->x<lim,ht_pga[1,:])

out_pga, groups = count_variable(ht_pga[:,tmp], tout,nbins)
imshow(out_pga./nruns*100,extent=[minimum(tout)/yr+1970,maximum(tout)/yr+1970,minimum(ht_pga),lim],cmap="Blues",aspect="auto")
cx=colorbar(ax=axb,shrink=0.0)
#cx.set_ticks(0.0)
cx=colorbar(shrink=0.95)
cx.set_label("percent of runs",rotation=270,labelpad=15)
ylabel("Proglacial \n sediment height \n"* L"($H_p$; m)")
xticks(rotation=90,fontsize=0.000001)
text(2014.25,.9*lim,"b",fontsize=14)

subplot(3,1,3,sharex=axb)
out_sga, _ = count_variable(ht_sga, tout,nbins)
imshow(out_sga./nruns*100,extent=[minimum(tout)/yr+1970,maximum(tout)/yr+1970,minimum(ht_sga),maximum(ht_sga)],aspect="auto",cmap="Reds")
cx=colorbar(shrink=0.95)
cx.set_label("percent of runs",rotation=270,labelpad=15)
plot(tout./yr .+ 1970 ,median(ht_sga,dims=2),"Black", lw=3)
ylabel("Subglacial \n till height \n"* L"($H_g$; m)")
xlabel("Year")
text(2014.25,.0065,"c",fontsize=14)

xlim([2014,2021])
#ylim([6e-3,1.1e0])

 tight_layout()
 subplots_adjust(hspace=0.0)
# show()

if save == true
    savefig("/Users/Ian/research/lumped_model/model_outputs/model_outputs_forward.pdf")
end

# labels = ["Dₘ \n (m)", "Dₘₚ\n (m)", "ϵ̇ \n (mm a⁻¹)", "ht0 g \n (m)", "ht0 p \n (m)",   "Δt \n (d)", "σ_error \n (m³) "]
# figure()
# for i = 1: 6
#     subplot(3,2,i)
#     plot(costs[:,1],theta[:,i], ".", color="black", lw=.1)
#     ylabel("KGE")
#     xlabel(labels[i])
# end
# if save == true
#     savefig("/Users/Ian/research/lumped_model/model_outputs/parameters_v_error.pdf")
# end

# clf()
# figure()
# plot([0,maximum(obs)], [0,maximum(model_outs)],"gray",lw=3)
# plot(obs,model_outs,".", color="black",lw=0.001)
# xlim([0, max(maximum(obs),maximum(model_outs))])
# ylim([0, max(maximum(obs),maximum(model_outs))])

# ylabel("Model outputs \n Sediment discharge  (m³ 5d⁻¹)")
# xlabel("Observations \n Sediment discharge  (m³ 5d⁻¹)")
# tight_layout()

# if save == true
#     savefig("/Users/Ian/research/lumped_model/model_outputs/model_correlation.pdf")
# end
