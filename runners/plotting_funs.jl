using JLD2
function load_runs(run_names)

    cost_out = []; theta_out = []
    for i = 1:length(run_names)
       
       
        @load "$(run_names[i]).jld2" cost thetas obs  max_min
        cost_ = cost
        thetas_ = thetas
        push!(cost_out,   cost)
        push!(theta_out, thetas)
    end
    
    cost_out = vcat(cost_out...)
    theta_out= vcat(theta_out...)

    return cost_out, theta_out
end


function count_variable(vari, tout,bin_no)

    groups = range(minimum(vari), maximum(vari), bin_no)
    out = zeros(bin_no,length(tout))

    for i =1:length(tout)
        for ii = 1:size(vari)[2]
            for jj = 1:length(groups)-1
                if vari[i,ii] > groups[jj] && vari[i,ii]  <= groups[jj+1]
                    out[jj,i] +=1
                end
                
            end
        end
    end
    out = reverse(out, dims = 1)

    return out,groups
end

