using DifferentialEquations,OrdinaryDiffEq
using Pkg
Pkg.activate(".")
using Parameters
using Random
using Distributions
using PyPlot
using StatsBase
using JLD2
using Dates
using Random

glacier = "fiescher_evol"
# glacier = "watson"
# glacier = "synth"

include("../src/interface_lumped.jl")
include("../src/interface-$(glacier).jl")
include("../src/$(glacier)_lump.jl")

obs = time_scale_sum(max.(1e-11,pg.sediment(pg.tout)),pg.tout,5*day,pg) #max added to account for negative values from splining
save =true 

Qws = Qw.(pg.tout[1:4:end],Ref(pg),Ref(pp))


@time glac_len = DS.(pg.tout[1:4:end],Ref(pg))./1000
@time pga_len  = DS_p.(pg.tout[1:4:end],Ref(pg))./1000

∇pga     = gradzb_fluvial.(pg.tout[1:4:end],Ref(1.0),Ref(pg),Ref(pp))
∇glac    = gradzs.(Ref(1), Ref(1), pg.tout[1:4:end], Ref(pg))

glac_th  = thick.(Ref(1),Ref(1),pg.tout[1:4:end], Ref(pg))
pga_th   = proglacial_top_height.(pg.tout[1:4:end])

figure("1",figsize=(5.5,4))
clf()
ax=subplot(2,1,1)
plot(pg.tout[1:4:end]./year .+ 1970, pga_len, "grey", lw=2)
ylabel("Proglacial area \n length \n (km)",color="black")
xticks([],rotation=90,fontsize=0.000001)
xlim([2014,2021])
xticks([],rotation=90,fontsize=0.000001)

plt=twinx()
plot([0; 0] ,[-999; -999],"grey",lw=2,label="Proglacial area")
plot(pg.tout[1:4:end]./year .+ 1970,glac_len,"black",lw=2,label="Glacier")
ylabel("Glacier length \n (km)",color="black",labelpad=35,rotation=270) 
ylim([13.65, 14.2])
xticks([],rotation=90,fontsize=0.000001)
text(2014.25,14.1,"a",fontsize=14)
legend(loc=7)

subplot(2,1,2,sharex=ax)
plot(pg.tout[1:4:end]./year .+ 1970,∇pga, "grey", lw=2)
xticks([],rotation=90,fontsize=0.00001)
ylabel("Proglacial area \n gradient \n (-)",color="black")
ylim([0.0345, 0.046])
yticks(round.(range(minimum(∇pga),maximum(∇pga),length=4),digits=3))

plt=twinx()
plot(pg.tout[1:4:end]./year .+ 1970,∇glac,"black",lw=2)
ylabel("Glacier surface \n gradient \n (-)",color="black",labelpad=35,rotation=270)
text(2014.25,0.0842,"b",fontsize=14)
xticks(collect(2014:2021),rotation=90,fontsize=0)
ylim([0.082, 0.0845])
yticks(round.(range(minimum(∇glac),maximum(∇glac),length=7),digits=3))



tight_layout()
subplots_adjust(hspace=0.0)

if save == true
    savefig("/Users/Ian/research/lumped_model/model_outputs/glacier_info.pdf")
end
