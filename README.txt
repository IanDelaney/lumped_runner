LUMPED_RUNNER- a lumped version of SUGSET to include geomorphic processes in glacier margins
================
Presented in Delaney I., M.A. Werder,D. Felix, R. Boes, I. Albayrak, D. Farinotti, 2023, Controls on sediment transport from a glacierized catchment in the Swiss Alps established through inverse modeling of geomorphic processes. Water Resources Research.
================
Copyright Ian Delaney 2023
Funded by Swiss National Science Foundation No. PZ00P2_202024

================
src contains code libraries
runners contains running scripts and plotting functions
	example.jl is an example case with synthetic hydrology
	lumped_runner.jl takes a fieschergletscher case
	lumped_forward.jl runs the 600 top performing parameter combinations from theta_fiescher_evol_select.jld2 and stores outputs in outputs_fiescher_evol_select.jld2
	lumped_glue.jl performs parameter search
	Fig8.jl creates Figure 8 from the paper and thresholds for parameter combinations can be selected at top of file. This script loads all_runs_thetas_costs.jld2

Fieschergletscher data to run model forward is linked in interface files.

================
For any questions, please contact ianarburua.delaney@unil.ch
