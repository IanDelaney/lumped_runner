#topography and hydrology input
using DelimitedFiles
using Interpolations
using Dierckx, ImageFiltering
using VAWTools
using OffsetArrays
using Statistics
import Dates: DateTime, datetime2unix
fp = "../data/"

##############     data/Wysswasser_Titer_Q_0_T_air_2014_2020_hourly.txt
Qw_read = readdlm("data/Wysswasser_Titer_Q_0_T_air_2014_2020_hourly.txt",'\t',String,skipstart=1) # read in runoff

SSC_read = readdlm("data/Wysswasser_Titer_Turbi_SSC_2014_2020_hourly.txt",'\t',String,skipstart=1) # read in runoff
SSC  = parse.(Float64,SSC_read[1:end,4])
Qw_data = parse.(Float64,Qw_read[1:end,2]); 
runoff_date_mod = datetime2unix.(DateTime.(Qw_read[1:end,1],"dd.mm.yyyy HH:MM"))

sediment = Qw_data.*SSC.*rs^-1 # note that rs in bedrock density.

###
#first column is time, second is length from image- take to the first emergence of water from glacier. third is the height elevation change from water intake estimate from Swiss topo map.
# 
###
pga_info= [1.3526784e9 640  28   # 12.11.2012  2012 Date from glamos and tiling image swiss topo- linear interpolation made in solver
           1.4121216e9 759  32   # 01.10.2014  assumed to be in fall and date say 2014 in tiling image swiss topo
           1.505952e9  996  34   # 21.09.2017   Date from glamos and tiling image swiss topo
           1.5991776e9 1020 34]  # 04.09.2020 date from glamos and tiling image swiss topo... DEM suggests covered in ice... height change assumes same  as 2017
 
include("interface-fiescher_evol.jl")

pn = LumpNum()
pg = LumpedRealGlacierEvol(tspan=(runoff_date_mod[20],runoff_date_mod[end]),
                           )
pg = LumpedRealGlacierEvol(pg,
                           ϵ̇=0.413e-3/year,
                           tout=pg.tspan[1]:0.25*day:pg.tspan[end],
                           int_Qw_sm= Spline1D(runoff_date_mod[20:end],
                                               Images.mapwindow( v->quantile(v, pg.source_quantile),
                                                                 Qw_data[20:end],
                                                                 (Int(div(pg.Q_sm/3600,2)*2+1),) # make odd
                                                                 )
                                               ;  k=1, bc="nearest")
                           )
pg = find_measures(pg,LumpedRealGlacierEvol)
pp=Phys(Dm=.041,
        d=30,
        hookeangle=π,
        fi=4,
        ft=4,
        fp=5,
        )

pn=LumpNum(Δσ=1e-3)
