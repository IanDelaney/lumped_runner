#################################
### create glacier parameters ###
###gg##############################
using Parameters, Dierckx, Interpolations, Images
using OffsetArrays, ForwardDiff


retreat_rate(t) = 100/year #-150/year 
thinning(t)= 2/year*(t-pg.tspan[1])
proglacial_slope(t) = 2*t/year ## 2 m up per year

DS(t,pg) = maximum(pg.domain_s) - (t-pg.tspan[1])*retreat_rate(t)
dl_dt(t,pg)= retreat_rate(t)

function DS_p(t,pg)
    out = maximum(pg.domain_s_p) + (t-pg.tspan[1])*retreat_rate(t)
    @assert DS(t,pg)+out ==  maximum(pg.domain_s) +  maximum(pg.domain_s_p)
    return out
end

"Defines lumped glacier"
@with_kw struct LumpSynthEvol <: Glacier #@deftype Float64
    # Time domain
    tspan::Tuple{Float64, Float64}
    tspan_spin::Tuple{Float64, Float64}=(0.0, 1.0)
    tout::Vector{Float64}=tspan[1]:0.25*day:tspan[2] # output times
    @assert tout[1]>=tspan[1]
    @assert tout[2]<=tspan[2]


    #Gorner
    domain_s::Tuple{Float64, Float64}=(0, 11320.0)
    domain_h::Tuple{Float64, Float64}=(0, 466.0)
    domain_s_p::Tuple{Float64, Float64}=(0, 3200.0)
    domain_h_p::Tuple{Float64, Float64}=(0, 466.0)
    
   
    name::String="lumped"
    
    ns::Int = Int(1) 
    nh::Int = Int(1)  
    
    #Gorner
    ds::Function = DS
    dh = 400.0 #
    ds_p::Function = DS_p
  
    dh_p = 400.0

    hv=dh./2

    fluvial::Bool = false

    S0 =5
    # IC & BC
    ht0::Vector{Float64}=[0.1, 0.2] # initial till layer thickness
    Qw_top = 0 # water discharge entering the domain at top
    Qb_top = 0 # bedload entering the domain at top

    # source averaging
    source_average_time=day*.75
    source_quantile = 0.75

    #till
    till_growth_lim = 1 # maximium amount of till, above which till stops being produced.
    fsl = 1.0# fraction of basal sliding
    till_lim = 100
    ϵ̇ = 1.5e-3/year
    
    hd=0.05
    vt=0 # terminus velocity
    
    # Topo
    para = 0.05
    spin::Bool = false
    spin_fac = 1
    # misc parameters
    float_fracˣ = 0.99
    float_frac::Vector{Float64} = float_fracˣ .+ 0.0*ht0 # 1<=>overburden pressure, 0<=>atmospheric
    
    #@assert 0<=float_frac<1

    gamma= -0.0008./year # mass balance gradient pe/r second
    B_0_term = 0
    Q_sm = 1 #smoothing factor for discharge  maw: this is awkward with units as changing the sampling rate will change this.  Better use window in units of time

end

#########################
### shape data
#########################

"""
                            makes surface
                            mean surface
                            """
function zs(s,h,t, pg::LumpSynthEvol)
    150-thinning(t)
end

"""
                            makes bed
                            one σ from mean thickness
                            """
zb(s,h,t, pg::LumpSynthEvol) =  0 # mean plus half a standard devaiation

function gradzs(s, h, t, pg)
    out = (1186+proglacial_slope(t))/(DS(t,pg))
    return out 
end

function gradzb(s, h, t, pg)
    out = (1186-zs(s,h,t, pg))/DS(t,pg)
    return out 
end

gradzb_fluvial(t,ht,pg,pp) = (150 +ht +proglacial_slope(t))/(DS_p(t,pg)) # 

function thick(s,h,t, pg::LumpSynthEvol)
    out = max( zs(s,h,t, pg::LumpSynthEvol) - zb(s,h,t, pg::LumpSynthEvol) ,0.0)
end

dht_dt_fn(ht,t, pg, pp, pn) = dht_dt_fn_lump(ht,t, pg, pp, pn) 

source_till(s,h,t,pg,pp) = pg.ϵ̇/year

function source_water_sm(s,h,t, pg, pp)
    sat = pg.source_average_time
    source_quantile = pg.source_quantile
    # average over some time
    dt = 2*3600 #2hours

    set = zeros(convert(Int,div(sat,dt)))
    for i = 1: length(set)
        tt = t-sat .+ dt*i
        set[i] = source_water(s, h, tt, pg,pp )
    end
    out = quantile(set, source_quantile)
end
function source_water(s,h,t,pg::LumpSynthEvol,pp)
    
    return max(0.0,((zs(s,h,t,pg)*pp.lr+day_var(t, pp)+temperature_d(t,pp)+ pp.ΔT)*pp.DDF)) + pp.basal
    
end
"""
    Daily temperature variations
    """
day_var(t, pp) = pp.DDAMP * cos(2*pi/day*t)

"""
        temperature(t, pp)

    Seasonal temperature.  Mid-winter at t=0.
    """
function temperature(t, pp)

    if typeof(pp.ΔT) == Float64
        ΔT = pp.ΔT
    else
        ΔT = pp.ΔT(t)
    end
    
    out =-pp.DAMP.*cos(2*pi/year*t)-5 + ΔT
end

"""
        temperature_d(t, pp)

    Daily and seasonal temperature.  Mid-winter/night at t=0.
    """
temperature_d(t, pp)=  temperature(t,pp) + day_var(t,pp)


function Qw( t::Number, pg::LumpSynthEvol, pp)
    s=DS(t,pg)/2; h=pg.hv
    out = -source_water(s,h,t,pg,pp)+ pg.Qw_top
    return out 
end

function Qwsm( t::Number, pg::LumpSynthEvol, pp)
    s=DS(t,pg)/2; h=pg.hv
    out = -source_water_sm(s,h,t,pg,pp) + pg.Qw_top
    return out
end

