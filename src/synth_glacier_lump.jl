#topography and hydrology input
using DelimitedFiles, Dates
using Interpolations
using Dierckx, ImageFiltering
#using VAWTools
using OffsetArrays
using Statistics

pg= LumpSynthEvol(tspan=(0*year,10*year))
pg= LumpSynthEvol(pg,
                  source_average_time=5*day,
                  ht0=[.0001],
                  S0= .5,
                  tout=pg.tspan[1]:0.25*day:pg.tspan[2],
                  ϵ̇=1e-3/year,
                  )

pp=Phys(Dm=.05,
        d=6,
        DDAMP=.5,      
        ΔT=2.0,
        fi = 10,
        ft = 10, 
        )

pn=LumpNum()

