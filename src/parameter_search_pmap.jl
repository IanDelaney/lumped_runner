# right now 4 parameters are included
using Distributions,ProgressMeter
using Random,DelimitedFiles
using Distributed, JLD2
if length(procs())==1
    addprocs(4)
end
@everywhere begin
    using Pkg
    Pkg.activate(".")

    using VAWTools, Parameters, Distributions, Random, DelimitedFiles,JLD2, ProgressMeter
    using DifferentialEquations,OrdinaryDiffEq,StatsBase
    
    glacier = $glacier

    include("interface_lumped.jl")
    include("interface-$(glacier).jl")
    include("$(glacier)_lump.jl")
    
    obs = $obs

    if homedir()== "/Users/Ian"
        fp_save = "/Users/Ian/julia/lumped_runner/"
    elseif homedir()=="/home/tesla-k20c"
        fp ="/home/tesla-k20c/ssd/iand/data/sed_modeling_data/$(glacier)/"
        fp_save="/home/tesla-k20c/ssd/iand/outputs/lump/"
    end
end

@eval @everywhere begin
    
    prob = (p) -> objfun(pg,pp,pn,p)
 
    pg=$pg

    max_min = $max_min
    sample_grab = (n) -> sampler_(max_min)
end


function parameter_search_lumped(prob,obs,max_min,total_runs)
    # run many model runs
    
    out = @showprogress pmap(nr->one_evaluation(nr,prob,obs), 1:total_runs;)    
    #out =  pmap(nr->one_evaluation(nr,prob,obs), 1:total_runs)    
    out = vcat(out...)
    cost = out[:,1:7]
    thetas = out[:,8:end]
    
    println("making output files  model runs finished... post_processing")
    
    return cost, thetas
end
 


@everywhere function one_evaluation(nr,prob,obs_)

    ts= obs_[pg.meas_inds,2]
    obs= obs_[pg.meas_inds,1]
    
    theta = sample_grab(nr)
    model_out = prob(theta)[pg.meas_inds]
    
    obs_year = time_scale_sum(obs,ts,year,pg)[:,1] 
    model_year = time_scale_sum(model_out,ts,year,pg)[:,1] 
    cor_spear_year = corspearman(model_year,obs_year)
    cor_spear = corspearman(model_out,obs)
    
    thresh = 1e-11
    log_obs = log.(max.(thresh,obs)).+1
    log_model = log.(max.(thresh,model_out)).+1
    
    abs_err = abs_error(model_out,obs)
    abs_err_log =  sum(log.( (abs.(model_out) .+ 1)./(abs.(obs) .+ 1)).^2) # log misfit
    abs_err_year = abs_error(model_year,obs_year)

    rel_err = sum(abs.((model_out .- obs)./obs))
   
    cost = [KGE(model_out,obs)  abs_err abs_err_log abs_err_year cor_spear cor_spear_year rel_err]
    
    theta_new = zeros(1,length(theta))
    
    theta_new[1]= theta[1];theta_new[2]= theta[2]
    theta_new[3]= theta[3];theta_new[4]= theta[4]
    theta_new[5]= theta[5];
    
    return [cost theta_new]
end

@everywhere function one_evaluation(nr,prob)
    
    theta = sample_grab(nr)
    model_out = prob(theta)[end] 
    
    return [model_out theta_new]
end
