#################################
### create glacier parameters ###
###gg##############################
using Parameters, Dierckx, Interpolations, Images
using OffsetArrays, ForwardDiff

DS(t,pg) = 11320 # length of glacier

"Defines lumped glacier"
@with_kw struct LumpSynthEvol <: Glacier #@deftype Float64
    # Time domain
    tspan::Tuple{Float64, Float64}
    tspan_spin::Tuple{Float64, Float64}=(0.0, 1.0)
    tout::Vector{Float64}=tspan[1]:0.25*day:tspan[2] # output times
    @assert tout[1]>=tspan[1]
    @assert tout[2]<=tspan[2]


    #Gorner
    domain_s::Tuple{Float64, Float64}=(0, 11320.0)
    domain_h::Tuple{Float64, Float64}=(0, 466.0)
    S0= 1

   
    name::String="lumped"
    
    ns::Int = Int(1) 
    nh::Int = Int(1)  
    
    #Gorner
    ds::Function = DS
    dh = 466.0 #

    hv=dh./2

    fluvial::Bool = false

    # IC & BC
        
    ht0::Vector{Float64}=[0.1 0.1] # initial till layer thickness
    Qw_top = 0 # water discharge entering the domain at top
    Qb_top = 0 # bedload entering the domain at top

    # source averaging
    source_average_time=day*.75
    source_quantile = 0.75

    #till
    till_growth_lim = 1 # maximium amount of till, above which till stops being produced.
    fsl = 1.0# fraction of basal sliding
    till_lim = 100
    ϵ̇ = 1.5e-3/year
    
    hd=0.05
    vt=0 # terminus velocity
    
    # Topo
    para = 0.05
    spin::Bool = false
    spin_fac = 1
    # misc parameters
    float_fracˣ = 0.99
    float_frac::Vector{Float64} = float_fracˣ .+ 0.0*ht0 # 1<=>overburden pressure, 0<=>atmospheric
    
    #@assert 0<=float_frac<1

    gamma= -0.0008./year # mass balance gradient pe/r second
    B_0_term = 0
    Q_sm = 1 #smoothing factor for discharge  maw: this is awkward with units as changing the sampling rate will change this.  Better use window in units of time

end

#########################
### shape data
#########################

"""
                            makes surface
                            mean surface
                            """
function zs(s,h,t, pg::LumpSynthEvol)
    150
end

"""
                            makes bed
                            one σ from mean thickness
                            """
zb(s,h,t, pg::LumpSynthEvol) =  0 # mean plus half a standard devaiation

function gradzs(s, h, t, pg)
    out = (1186)/(DS(t,pg))
    return out 
end

function gradzb(s, h, t, pg)
    out = (1186-zs(s,h,t, pg))/DS(t,pg)
    return out 
end


function thick(s,h,t, pg::LumpSynthEvol)
    out = max( zs(s,h,t, pg::LumpSynthEvol) - zb(s,h,t, pg::LumpSynthEvol) ,0.0)
end

dht_dt_fn(ht,t, pg, pp, pn) = dht_dt_glacier(ht,t, pg, pp, pn) 

source_till(s,h,t,pg,pp) = pg.ϵ̇/year

function source_water_sm(s,h,t, pg, pp)
    sat = pg.source_average_time
    source_quantile = pg.source_quantile
    # average over some time
    dt = 2*3600 #2hours

    set = zeros(convert(Int,div(sat,dt)))
    for i = 1: length(set)
        tt = t-sat .+ dt*i
        set[i] = source_water(s, h, tt, pg,pp )
    end
    out = quantile(set, source_quantile)
end
function source_water(s,h,t,pg::LumpSynthEvol,pp)
    return max(0.0,((zs(s,h,t,pg)*pp.lr+day_var(t, pp)+temperature_d(t,pp)+ pp.ΔT)*pp.DDF)) #+ pp.basal
    
end
"""
    Daily temperature variations
    """
day_var(t, pp) = pp.DDAMP * cos(2*pi/day*t)

"""
        temperature(t, pp)

    Seasonal temperature.  Mid-winter at t=0.
    """
function temperature(t, pp)

    if typeof(pp.ΔT) == Float64
        ΔT = pp.ΔT
    else
        ΔT = pp.ΔT(t)
    end
    
    out =-pp.DAMP.*cos(2*pi/year*t)-5 + ΔT
end

"""
        temperature_d(t, pp)

    Daily and seasonal temperature.  Mid-winter/night at t=0.
    """
temperature_d(t, pp)=  temperature(t,pp) + day_var(t,pp)


function Qw( t::Number, pg::LumpSynthEvol, pp)
    s=DS(t,pg); h=pg.hv
    out = -source_water(s,h,t,pg,pp)*pg.ds(t,pg)*pg.dh + pg.Qw_top
    return out 
end

function Qwsm( t::Number, pg::LumpSynthEvol, pp)
    s=DS(t,pg)/2; h=pg.hv
    out = -source_water_sm(s,h,t,pg,pp)*pg.ds(t,pg)*pg.dh + pg.Qw_top
    return out
end

function run_model(pg, pp::Phys, pn::LumpNum; verbose=false)
    @unpack  ht0, S0, tspan, tout = pg

    verbose && (print("Start integration ... "); tic())

    objfn = (y,p,t) -> dht_dt_fn(y, t, pg, pp, pn)[1]
    prob = ODEProblem(objfn, [ht0[1], S0], tspan)

    sol = solve(prob,
                pn.alg;
                pn.odeopts...)

    verbose && (println("integration finished"); toc())

    return sol
end
