using RandomNumbers
using ForwardDiff
using Parameters
using Distributions
using DifferentialEquations

# Misc. well-known, fixed physical constants
const day = 24*3600.  # secs in day
const year = day*365 # secs in year
const g = 9.81 # grav. accel.
const proglacial = 5
const rw = 1000.  # dens water
const ri = 917.   # dens ice
const rs = 2650.
@with_kw struct LumpNum @deftype Float64
    # ODE solver,
    # http://docs.juliadiffeq.org/latest/solvers/ode_solve.html
    alg::Any= VCABM()
    # Solver options
    # http://docs.juliadiffeq.org/latest/basics/common_solver_opts.html
    odeopts::Dict{Symbol,Any} =
        Dict{Symbol,Any}(
            :abstol=>1e-7, 
            :reltol=>1e-7,
            :dtmin=> 1,
            :dtmax=> 0.25*day, # maximum step size.  Set such that all temporal forcing is resolved.
            :save_everystep=>true # might make faster...
        )
    # misc smoothers, may make solver faster:
    Δσ = 1e-3 # smooth mobilization when going from transport to supply limited over this range of ht [m]
    max_delta_xsect = [0, 0.05][2]
    max_delta_MPM = [0, 0.001][1]
    max_delta_dht_dt_fn = [0, 1e-13][1]
    tstep = day
end

@with_kw struct Phys @deftype Float64
    ## Hydraulic
    e = .3 #empirical constants for channel widht
    d = 6.0 #empirical constants for channel width

    fi = 10	# assuming the same as above
    fp = 5

    xsec_min = .75 # minimum value of channel xsect
    hookeangle=π/3
    n = 3 # Glens
    C₁ = 2.2e-5
    C₂ = 3.7e-13
    sinuosity = 1
    
    ## Bedload transport
    rs = 2650 	# dens sed
    R  = (rs-rw)/rw # specific gravity of submerged sediment
    Dm = .01	# median grain size of sed.
    formula::Symbol = [:MPM,:W_C, :E_H][end]
    
    
    ## Melt model
    lr = -0.0075  #K/m lapse rate
    ΔT::Any = 10.0 # temp offset
    DAMP = 16 # temp amplitude
    DDAMP = 2
    DDF = 0.01/day # degree day factor
    DDF_Qdet = 0.0000001
    basal= 7.93e-11 #wm/s
end
abstract type Glacier end


source_till(t,pg,pp) = pg.ϵ̇

function Qw( t::Number, pg, pp)
    s=pg.sv; h=pg.hv
    out = -source_water(s,h,t,pg,pp)*pg.ds*pg.dh + pg.Qw_top
    return out 
end

channel_width_fluvial(Qw,pp)= pp.d*Qw^pp.e

function tau_fluvial(Qw,ht,t,pg,pp)

    wc  = channel_width_fluvial(Qw,pp)
    tau = gradzb_fluvial(t,ht,pg,pp)^(2/3) * (Qw/wc)^(2/3) * (rw*g^(2/3)*pp.fp^(1/3))/2  #Tucker and Slingerland 1997 Eq 8
    
    return tau,wc
end

function gradzb_fluvial(t,ht,pg,pp) end

function run_model(pg, pp::Phys, pn::LumpNum; verbose=false)
    @unpack  ht0, S0, tspan, tout = pg

    verbose && (print("Start integration ... "); tic())

    objfn = (y,p,t) -> dht_dt_fn(y, t, pg, pp, pn)[1]
    prob = ODEProblem(objfn, [ht0[1], ht0[2],  s_spin(pg,pp)], tspan)

    sol = solve(prob,
                pn.alg;
                pn.odeopts...)

    verbose && (println("integration finished"); toc())

    return sol
end


function test_thetaref(theta_ref,prange)
    for i=1:length(theta_ref)
        @assert theta_ref[i] < maximum(prange[i,:]) "index $i is  too big"
        @assert theta_ref[i] > minimum(prange[i,:]) "$i th index is  too small"
    end
    
end

"""
     ∂S_∂t(S,t,pg,pp)

     Change in channel size
"""
function ∂S_∂t(S,t,pg,pp)
    
    s=S[1]
    lc=pp.sinuosity*DS(t,pg)
    Q  = abs(Qw(t,pg,pp))
    Ψ_ = Ψ(Q,s,pg,pp)/(rw*g)
    
    hice = ri/rw*thick(1,1,t, pg)
    
    Δh = lc*(Ψ_)
    N = hice - Δh/2
    
    dS = pp.C₁ * (Q*Δh)/lc  - s*pp.C₂*(N)^pp.n

    return dS, Δh, -Q
end


"""
 Ψ(Q,S,pg,pp)

 Darcy-Weisbach as function of Q(water discharge), S(size) parameters
"""
Ψ(Q,S,pg,pp) = hookefact(pp.hookeangle) * pp.fi * rw * (Q^2)/(S2Dh(S,pp.hookeangle)^5)


function time_scale_sum(timeseries::Vector{Float64},time_knots::Vector{Float64},timestep::Float64, pg)

    output= zeros(Int((time_knots[end]-time_knots[1])÷timestep),1)
    ts = zeros(Int((time_knots[end]-time_knots[1])÷timestep),1)
    dt = time_knots[2] -time_knots[1]
    index_per_step = Int(length(time_knots) ÷ length(output))

    for i = 0:length(output)-1
        pt_a=i*index_per_step+1
        pt_b=(i+1)*index_per_step
        output[i+1] = sum(timeseries[pt_a:pt_b]*dt)
        ts[i+1] = time_knots[(pt_a+pt_b)÷2]
    end

    return [output  ts]
end


function  pull_thetas(theta_new)
    x=unique(theta_new,dims=1)
    return x 
end

function sampler_(max_min)
    out = (rand(RandomNumbers.Xorshifts.Xoroshiro128Star(), Uniform(max_min[1,1],max_min[1,2])),  #Dm
           rand(RandomNumbers.Xorshifts.Xoroshiro128Star(), Uniform(max_min[2,1],max_min[2,2])),  #ϵ̇
           rand(RandomNumbers.Xorshifts.Xoroshiro128Star(), LogUniform(max_min[3,1],max_min[3,2])),  #ht0
           rand(RandomNumbers.Xorshifts.Xoroshiro128Star(), LogUniform(max_min[4,1],max_min[4,2])),  #ht0
           rand(RandomNumbers.Xorshifts.Xoroshiro128Star(), LogUniform(max_min[5,1],max_min[5,2]))   #Hd
           )
    
    return out
end

function mass_con_check(sol,pg,pp,pn::LumpNum,tspan)


  @unpack   ds, ds_p, ns, dh, dh_p, nh = pg
    tout = sol.t
    dt=diff(sol.t)
    # volume change in till layer
    Δhts = sol(tspan[2]) - sol(tspan[1])
    ΔV = Δhts[1].*dh,tspan[1].*Dierckx.evaluate(ds,tspan[1]) + Δhts[2].*dh_p.*Dierckx.evaluate(ds_p,tspan[2])

    V_source = 0.0
    V_progl = 0.0
    
    # integrate in time:   
    for (k,t) in enumerate(tout[1:end-1]) 
        ht = sol(t)
        
        Qb = dht_dt_fn(ht,  t, pg, pp, pn)[2]
        V_progl +=  Qb[end]*dt[k]
        
        #verbose && println("sediment discharge: $(V_progl)")
        
        V_source += effective_sed_source(pg.sv, pg.hv, t, pg, pp, pn, ht[1]) .* dh .*ds .*dt[k]
    end

    return V_progl, ΔV, V_source
end

"""
                                    E_H( t::Number, pg, pp, pn)

                                Engelund and Hansen 1967... Total sediment transport relationship.

                                https://www.engr.colostate.edu/~pierre/ce_old/Projects/CASC2D-SED%20Web%20site%20082506/CASC2D-SED-Sediment.htm
                                http://sic.g-eau.net/engelund-hansen-1967,1033?lang=en
                                https://www.tugraz.at/fileadmin/user_upload/Institute/IWB/Lehre/Software/BedLoadAnalyzer/Referenzhandbuch.pdf
                                """
function E_H(tau,pg, pp, pn)
    @unpack R,fi,Dm,rs = pp
    
    velo = -sqrt(8*tau/fi)
    dir = sign(velo)
    # E.g. https://www.tugraz.at/fileadmin/user_upload/Institute/IWB/Lehre/Software/BedLoadAnalyzer/Referenzhandbuch.pdf
    # equation 45
    qb = dir* (0.05*8/fi) * (1/(Dm*R^2*g^2)) *(tau/rw)^(5/2)

    return  qb
end

"""
W_C(Q,S,pg,pp)

"""
function W_C(Q,S,pg,pp)

    @assert pp.Dm >4e-3 "Wilcock and Crowe not valid for grain sizes less that 4mm" # conditional statement 

    dir = sign(Q)
    Ψˣ = max(1e-10,Ψ(Q,S,pg,pp))/(rw*g)  # Find the slope
    rh = S2Dh(S, pp.hookeangle)/4  #hydraulic radius is the hyraulic diameter  divided by 4: https://en.wikipedia.org/wiki/Hydraulic_diameter#:~:text=The%20Manning%20formula%20contains%20a,for%20calculations%20involving%20turbulent%20flow.
    
    τ  = rh*Ψˣ/(pp.R*pp.Dm) #Equation 7 Antoniazza et al., 2022
    τᵣ = 0.56*Ψˣ^.5         #Equation 8 Antoniazza et al., 2022

    τˣ= τ/τᵣ
    if τˣ < 1.143
        Wˣ = 0.002(τˣ)^16.1
    else τˣ >= 1.143
        extra = (0.85/((τˣ)^.7))
        
        Wˣ = 14*(1-extra)^4.5
    end

    out = dir* rs * Wˣ * (g*rh*Ψˣ)^1.5 / (pp.R*g) # note that width scaling "b" in Antoniazzia et al. 2022 has been removed to be count in sed_transport-> this has units kg/s
    
    return out/rs 
end



"""
                          sed_transport(t::Number, pg,pp)

                          Equilibrium bedload transport rate [m³/s]

                          Select sediment transport relationship by setting pp.formula.

                          The called functions should give transport per unit channel width in physical (SI) units.
                        """
function sed_transport(Qw, S, pg, pp, pn) #make universal so other sed. transport formulas can be added.
    
    wc=channel_width(S, pp.hookeangle)
    
    v=(Qw./S)
    if v>1.3
        println(v)
    end
        τ=shr_str_w(v, pg, pp, pn)
    qbe = if pp.formula==:MPM
        MPM(τ, pg, pp, pn)
    elseif pp.formula==:E_H
        E_H(τ, pg, pp, pn)
    elseif pp.formula==:W_C
        W_C(Qw, S, pg, pp)
    else
        error()
    end
    
    return qbe.*wc
end

"""
                                Sediment source function.  Goes linearly to zero as ht->pg.till_growth_lim.
                                """
function effective_sed_source(ht, t, pg, pp, pn)
    @unpack till_growth_lim, spin, spin_fac = pg
   
    out = (1-(ht/till_growth_lim))*source_till(t, pg, pp)
    if ht >= till_growth_lim
        out = 0
    end   
    return out
end

"Hyd diameter from Darcy-Weisbach"
Dh(dphi, Q⁺, pg, pp) = (pp.fi * 8*rw*(Q⁺)^2/abs(dphi) * hookefact(pp.hookeangle) )^(1/5)

"Convert hyd. diameter to cross-sectional area"
function Dh2S(Dh, hookeangle)
    (Dh/4)^2 * (sqrt(2) * (hookeangle + sqrt(2 - 2*cos(hookeangle))))^2 /
        (hookeangle - sin(hookeangle))
end

"Convert cross-sectional area to hyd. diameter"
function S2Dh(S, hookeangle)
    4* sqrt(S*(hookeangle - sin(hookeangle))) /
        (sqrt(2) * (hookeangle + sqrt(2 - 2*cos(hookeangle))))
end

"""
                                To account for Hooke-channels in the Darcy-Weisbach equation.

                                For a circle this is 1/pi^2, for semi-circle 4*pi^2/(pi+2)^4.
                                """
hookefact(hookeangle) = ( 2*(hookeangle - sin(hookeangle)) /
                          (hookeangle + 2*sin(hookeangle/2))^2 )^2

"""
                                Width of channel floor for a given cross-sectional area.
                                """
channel_width(S, hookeangle) = 2*sqrt(2*S/(hookeangle-sin(hookeangle)))*sin(hookeangle/2)

"""
                  shr_str_w( Qw,S, pg, pp, pn)

                Shear stress of water on sediment according to Darcy-Weisbach.

                """
function shr_str_w(v, pg, pp, pn)
    @unpack fi, xsec_min = pp

    tau = 1/8*fi*rw *  v^2
    
    return tau
end

"""
                                    # Mobilization (volume per unit time per unit channel lenxgth) occurs if transport
                                            # is below the transport capacity (otherwise deposition happens).
                                            # This corresponds to the dQb/dx term in the Exner equation.
                                            #
                                            # Note:
                                            # - we use the sediment flux a the upstream, here a boundary condition
                                            #   boundary to calculate cell-average mobilization.  This is a form up upwinding.
                                            # - porosity is taken as zero
                                            
                                            Qb_return(Qbe,Qin,hti,mt,thickness,pg,pp,pn)
                                     
                            """
function Qb_return(Qbe,Qin,hti,mt,t,ds,dh,pg,pp,pn::LumpNum)

    @assert Qin>=0
    @assert mt>=0
    @assert Qbe >=0 "$(Qbe) at time $(t)"
    
    mobilization_ = (Qbe - Qin)/ds#[m2/s]

    trans= 1
    # Condition A (transport limited) 
    if mobilization_<=mt*dh # transport limited for sure
        mobilization = mobilization_
        condition = "A"
    else# Either transport or supply limited,
        # make a smooth transition between the two:
        
        trans=sigmoid(hti, 0+2*pn.Δσ, pn.Δσ)
        
        mobilization =mobilization_*trans + mt * ds* (1-trans) #[m2/s]
        condition = "B"    
    end
    
    Qout = Qin + mobilization*dh ## [m3/s]   sed-flux out of cell 

    @assert Qout>=0 "Qin:  $(Qin)...  Qout: $(Qout)... mobilization: $(mobilization)"
    
    dht_dt_out = (Qin-Qout)/(dh*ds) + mt # [m/s] 
    
    return -Qout, dht_dt_out
end


function Qb_debris(ht,t,pg,pp)
    dadt= dl_dt(t,pg) * pg.dh
    if dadt >0 # case of retreat
        return (dadt+pg.vt)*pg.hd, dadt*ht # retreat rate × glacier width × thickness(englacial), thickness(subglacial)
    else # case of glacier advance
        return 0, dadt*ht # advance rate × glacier width × thickness(englacial), thickness(subglacial)
    end
end

"""

       dH_dt_fn_retreat(ht,t,pg)

       turns ht in to volume and scales with changing proglacial area to conserve mass
"""
function dH_dt_fn_retreat(ht,t,l,dldt,pg)
    vol    = ht * l
    new_ht = (vol/(l + dldt))  
    dh_dt_retreat = (new_ht - ht)    
    return dh_dt_retreat, vol
end

function dht_dt_glacier(ht,t, pg, pp, pn)
    @unpack  Qb_top = pg

    S = ht[2] 
    hts = ht[1]
    dS,_, q_w = ∂S_∂t(S,t,pg,pp)
    
    ### Subglacial ###
    Sˣ= max_smooth(pp.xsec_min,S,pn.max_delta_xsect) # make minimum (note that this is a fudge and does not propogate to ds calcualtion)

    Qbe_g =  sed_transport(q_w, # water discharge
                           Sˣ, # channel size
                           pg, pp, pn)
    
    mt= effective_sed_source(hts, t, pg, pp, pn)
    Qb_g, dht_dt_= Qb_return(-Qbe_g,
                             Qb_top,
                             hts, mt, t, DS(t,pg), pg.dh, pg, pp, pn)
    
    return [dht_dt_,dS], Qb_g, Qbe_g , q_w
end


"""
        # Mobilization (volume per unit time per unit channel lenxgth) occurs if transport
                                                    # is below the transport capacity (otherwise deposition happens).
                                                    # This corresponds to the dQb/dx term in the Exner equations.
                                                    #
                                                    # Note:
                         dht_dt_fn(ht,  t, pg, pp, pn)

                                Rate of change of till layer thickness (and bedload flux).
"""
function dht_dt_fn_lump(ht,t, pg, pp, pn) 
    @unpack  Qb_top = pg
    ### Retreat ###
    Qb_dhe,Qb_dhg = Qb_debris(ht[1],t,pg,pp)

    Qb_d = Qb_dhg + Qb_dhe # add together englacial and subglacial components
    @assert Qb_d >=0

    ### Glacier ###    
    dht_dt_, Qb_g, Qbe_g , q_w=  dht_dt_glacier([ht[1] ht[3]],t, pg, pp, pn)
    dS = dht_dt_[2]   
    dht_dt_=     dht_dt_[1]

    dldt=dl_dt(t,pg)
    dhdt_scaling,_ = dH_dt_fn_retreat(ht[1],t,DS(t,pg),-dldt,pg) 
    @assert sign(dldt) != dhdt_scaling || sign(dldt) == 0
    dht_dt_g = dht_dt_  -  Qb_dhg/(DS(t,pg)*pg.dh) + dhdt_scaling  # add it all together
    
    ### Proglacial ###
    pp= Phys(pp,
             fi= pp.fp,
             )

    tau, wc = tau_fluvial(-q_w,ht[2],t,pg,pp)
    Qbe_p = E_H(tau, pg, pp, pn)*wc
   
    Qb_p, dht_dt_= Qb_return(-Qbe_p,
                             -Qb_g, # from glacier (above)
                             ht[2],
                             0.0, # mt==0
                             t,
                             DS_p(t,pg), pg.dh_p,pg, pp, pn)
    
    dhdt_scaling,_ = dH_dt_fn_retreat(ht[2],t,DS_p(t,pg),dldt,pg)
    @assert sign(-dldt) != dhdt_scaling || sign(dldt) == 0

    dht_dt_p = dht_dt_ + Qb_d/(DS_p(t,pg)*pg.dh_p) +   dhdt_scaling # add it all together again
  
    
    ####################
    return [dht_dt_g, dht_dt_p, dS], [Qb_g, Qb_p], [Qbe_g, Qbe_p], Qb_d, q_w
end


"""
                                NSE(model,data)

                            Nash-Sutcliff Efficiency Coefficent

                            returns values between -Inf and 1, one begin perfect fit.
                            """
function NSE(model,data)

    E = abs.(model)-abs.(data)
    SSE = sum(E.^2)
    U = mean(abs.(data))

    SSU = sum((abs.(data) .- U).^2)

    out = 1-SSE/SSU

    return out
end

"""
                                        abs_error(model,data)
                                    """
function abs_error(model,data)

    out = sum(abs.(model - data))

end

function KGE(model,data)

    r = cor(model,data)[1]
    α = std(model)/std(data)
    β = mean(model)/mean(data)
    
    return 1 - ((r - 1)^2 +
                (α - 1)^2 +
                (β - 1)^2
                )^(1/2)
end
max_smooth(x0, x, delta) =
    delta==0 ? max(x0,x) : hyperbola01(x, delta, x0, x0)

"""
    sigmoid(x, x0, w)

Smooth transition function from 0 to 1.  At x0+w its value is about 0.99,
at x0-w about 0.01.
"""
sigmoid(x, x0, w) = 1/(1+exp(-(x-x0)/w*5))

"""
    hyperbola01(x, Δy, xoff=0.0, yoff=0.0)

A hyperbola with asymptotes y=0 for x<0 and y=x for x>0, and
abcissa Δy.  Its origin can be moved with xoff and yoff.
"""
function hyperbola01(x, Δy, xoff=0.0, yoff=0.0)
    x = x-xoff
    B = 1/2
    C = -Δy^2
    D = -1/4
    return B*x + sqrt(-D*x^2-C) + yoff
end

function find_measures(pg, Glacier)
    seds = (pg.sediment.(pg.tout))
    data = time_scale_sum(seds,pg.tout,pg.time_agg,pg)[:,1]

    inds = []
    for i in eachindex(data)
        if data[i]/(pg.time_agg) > 0.000001 #sed flux greater than 0.0001 m³ s⁻¹
            push!(inds,i) 
        end
    end
    pg =  Glacier(pg,
                  meas_inds=inds)
    return pg
end

function s_spin(pg,pp)
    global S = pg.S0
    global ∂S =0

    Qw_out = abs.(Qw.(pg.tout[1]:3600:pg.tout[1]+day*2,Ref(pg),Ref(pp)))  #Find water discharge

    for i = 1:15000
        # Update ∂S
        ∂S = ∂S_∂t(S,pg.tout[argmax(Qw_out)], pg, pp)[1] # tune ds to max value in first three days (i.e. index of 12 above)
      
        S += ∂S * .1*day   # Update S using Forward Euler method
    end
    return S
end


"""
 run_glacier_prob(pg, pp, pn)

  run glacier problem as function of pg and pn
"""
function run_hydro_prob(pg,pp, pn)
    
    objfn = (y,p,t) -> ∂S_∂t(y,t,pg,pp)[1]
    prob = ODEProblem(objfn, s_spin(pg,pp), pg.tspan)
    
    sol = solve(prob,
                pn.alg;
                pn.odeopts...)


    return sol
end
