#topography and hydrology input
using DelimitedFiles, Dates
using Interpolations
using Dierckx, ImageFiltering
#using VAWTools
using OffsetArrays
using Statistics

pg= LumpSynthEvol(tspan=(0*year,10*year))
pg= LumpSynthEvol(pg,
                  source_average_time=5*day,
                  ht0=[.0015, .0015],
                  tout=pg.tspan[1]:0.25*day:pg.tspan[2],
                  ϵ̇=1e-3/year,
                  )

pp=Phys(Dm=.005,
        d=6,
        DDAMP=.5,      
        ΔT=2.0,
        fi = 4,
        ft = 4, 
        fp=15,
       )
pn=LumpNum()

