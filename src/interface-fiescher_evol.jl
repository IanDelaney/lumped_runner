#################################
### create glacier parameters ###
#################################
using Parameters, Dierckx, Interpolations, Images
using OffsetArrays, QuadGK

thinning_rate(t)= 2/year*(t-pg.tspan[1])
proglacial_top_height(t) = pg.dH_p(t)
function DS(t,pg)
    pg.ds(t)
end

function dl_dt(t,pg)
    out = -Dierckx.derivative(pg.ds,t)
    return out 
end

function DS_p(t,pg)

    out =  maximum(pg.domain_s) +  maximum(pg.domain_s_p)- DS(t,pg)
    @assert DS(t,pg)+out ≈  maximum(pg.domain_s) +  maximum(pg.domain_s_p)
    return out
end



"Defines Real glacier"
@with_kw struct LumpedRealGlacierEvol <: Glacier @deftype Float64
    # Time domain
    tspan::Tuple{Float64, Float64}
    tspan_spin::Tuple{Float64, Float64}=(0.0, 1.0)
    tout::Vector{Float64}=tspan[1]:0.25*day:tspan[2] # output times
    @assert tout[1]>=tspan[1]
    @assert tout[2]<=tspan[2]


    #Fiescher
    domain_s::Tuple{Float64, Float64}=(0,14100.0)
    domain_s_p::Tuple{Float64, Float64}=(0, 640.0) # size of pga assuming linear retreat between 2011 and 2014 for pga size in 1.1.2014
    
    name::String="lumped"
    
    ns::Int = Int(1) 
    nh::Int = Int(1)  
    
    #Fiescher
    ds::Dierckx.Spline1D =  Spline1D(pga_info[:,1], 14100.0 .- pga_info[:,2] .+ pga_info[1,2];  k=1, bc="extrapolate") 
    dh = 2200 # 
    ds_p::Dierckx.Spline1D = Spline1D(pga_info[:,1], pga_info[:,2];  k=1, bc="extrapolate")
    dh_p = 200
    dH_p::Dierckx.Spline1D = Spline1D(pga_info[:,1], pga_info[:,3];  k=1, bc="extrapolate") # height change
    
    sv=2
    hv=2

    # IC & BC
    ht0::Vector{Float64}=[0.1, 0.2] # initial till layer thickness
    Qw_top = 0 # water discharge entering the domain at top
    Qb_top = 0 # bedload entering the domain at top



    doy_s = 152 # jun 1
    doy_f = 258 # sept 15
    
    #till
    till_growth_lim = 0.05 # maximium amount of till, above which till stops being produced.
    ϵ̇ = 1.5e-3/year
    hd = 0.0001
    vt=0 # terminus velocity

    # Topo
    para = 0.05
    spin::Bool = false
    spin_fac = 1
    # misc parameters
    float_fracˣ = 0.99
    
    
    # source averaging

    source_quantile = 0.75
    Q_sm = 5*day #smoothing factor for discharge  

    int_Qw::Dierckx.Spline1D    = Spline1D(runoff_date_mod[15:end], Qw_data[15:end];  k=1, bc="nearest") #w= ones(length(runoff_date_mod)),
    int_Qw_sm::Dierckx.Spline1D = Dierckx.Spline1D(runoff_date_mod[15:end],
                                                   Images.mapwindow( v->quantile(v, source_quantile),
                                                                     Qw_data[15:end],
                                                                     (Int(div(Q_sm/3600,2)*2+1),) # make odd
                                                                     )
                                                   ;w= ones(length(runoff_date_mod[15:end])),k=1, bc="nearest")

    sediment::Dierckx.Spline1D= Spline1D(runoff_date_mod[15:end], sediment[15:end]
                                         ; w= ones(length(runoff_date_mod[15:end])), k=1, bc="nearest")
    meas_inds::Vector{Int64} = zeros(length(10))
    time_agg = 5*day
end

#########################
### shape data
#########################

"""
                                        makes surface
                                        mean surface
                                        """
zs(s,h,t, pg::LumpedRealGlacierEvol) = 1186 # elevation diff of glacier 


"""
  makes bed
zb(s,h,t, pg::LumpedRealGlacierEvol)
evolves with retreat
"""
zb(s,h,t, pg::LumpedRealGlacierEvol) = pg.dH_p(t)

function gradzs(s, h, t, pg)
    out = (zs(s,h,t, pg)-zb(s,h,t, pg))/(DS(t,pg)) ## 1186 is elevation diff of glacier at start of model run (zs-zb(t))
    return out 
end

function gradzb(s, h, t, pg)
    out =  (zs(s,h,t, pg)-zb(s,h,t, pg)-thick(s,h,t,pg))/(DS(t,pg)) ## 1186 is elevation diff of glacier at start of model run (zs-zb(t))
    return out 
end

gradzb_fluvial(t,ht,pg,pp) = (zb(1,1,t, pg)+ ht )/(DS_p(t,pg)) # 

thick(s,h,t, pg::LumpedRealGlacierEvol) =225-thinning_rate(t)

function Qw( t::Number, pg::LumpedRealGlacierEvol, pp)
    if pg.spin == true
        out = -max.(pp.basal, pg.int_Qw_spin(t))
    elseif pg.spin == false
        out = -max.(pp.basal, pg.int_Qw(t))
    else
        error("spin or not!!!!")
    end
    return out 
end

function Qwsm( t::Number, pg::LumpedRealGlacierEvol, pp)
    if pg.spin == true
        out = -max.(pp.basal, pg.int_Qw_sm_spin(t))
    elseif  pg.spin == false
        out = -max.(pp.basal, pg.int_Qw_sm(t))
    else
        error("spin or not!!!!")
    end
    return out
end






dht_dt_fn(ht,t, pg, pp, pn) = dht_dt_fn_lump(ht,t, pg::LumpedRealGlacierEvol, pp, pn) 


"""
            Run model to extract cost function

            objfun_turing(pg,pp,pn,paras,data)
            """
function objfun(pg,pp,pn,paras; forward=false)

    Dm, ϵ, ht0_g, ht0_p, hd = paras

    
    ## redefine variables;
    pp = Phys(pp,
              Dm  =  Dm[1],
           )
    
    pg = LumpedRealGlacierEvol(pg,
                               ht0=[ht0_g, ht0_p],
                               ϵ̇ = ϵ[1],
                               hd= hd
                               )
    
    sol = run_model(pg, pp, pn)
    
    
    ## post-processing
    hts=sol(pg.tout)
    hts = hcat(hts...)'

    dt   = pg.tout[2]-pg.tout[1]
    Qbs  = zeros(length(pg.tout),2)
    if forward==true
        Qbes = zeros(length(pg.tout),2)
        Qb_d = zeros(length(pg.tout),1)
    end
    
    for (k,t) in enumerate(pg.tout)
        ht = sol(t)
        Qbs[k,:] = -dht_dt_fn_lump(hts[k,:], t, pg, pp, pn)[2]
        if forward == true
            Qbes[k,:] = -dht_dt_fn_lump(hts[k,:], t, pg, pp, pn)[3]
            Qb_d[k]   = -dht_dt_fn_lump(hts[k,:], t, pg, pp, pn)[4]
        end
    end

    model =  time_scale_sum(Qbs[:,2],pg.tout,pg.time_agg,pg)[:,1]
    
    if minimum(hts) < 0 # check if something funny is happening here... can occur at very small grain sizes
        model .= -Inf
    end

    if sol.t[end] != pg.tspan[2] # assert model makes it to the end
        model .= -Inf
    end

    
    if forward == true
        return model,  Qbs, hts, Qbes, Qb_d
    else
        return model
    end
end

